package ro.bogdan.serverrmi.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class Sensor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String uid;
    private String description;
    private Integer max_value;
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "sensor",orphanRemoval = true)
    private Set<MonitoredData> monitoredDataSet;


    public void addData(MonitoredData monitoredData){
        monitoredDataSet.add(monitoredData);
    }
}
