package ro.bogdan.serverrmi.resource;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.bogdan.serverrmi.dto.MonitoredDataDto;
import ro.bogdan.serverrmi.exception.SensorNotFoundException;
import ro.bogdan.serverrmi.model.MonitoredData;
import ro.bogdan.serverrmi.model.Sensor;
import ro.bogdan.serverrmi.repository.MonitoredDataRepository;
import ro.bogdan.serverrmi.repository.SensorRepository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ServerResource {

    private final MonitoredDataRepository monitoredDataRepository;
    private final SensorRepository sensorRepository;
    private final ModelMapper modelMapper;


    @GetMapping("/data/{sensorUid}")
    public Set<MonitoredDataDto> getAllDataForSensor(@PathVariable String sensorUid){
        Sensor sensor = sensorRepository.findByUid(sensorUid).orElseThrow(()-> new SensorNotFoundException(sensorUid));
        return sensor.getMonitoredDataSet().stream().map(this::fromEntityToDto).collect(Collectors.toSet());
    }
    private MonitoredDataDto fromEntityToDto(MonitoredData monitoredData){
        MonitoredDataDto monitoredDataDto = modelMapper.map(monitoredData,MonitoredDataDto.class);
        if(monitoredData.getSensor()!=null)
            monitoredDataDto.setSensorUid(monitoredData.getSensor().getUid());
        return monitoredDataDto;
    }


}
