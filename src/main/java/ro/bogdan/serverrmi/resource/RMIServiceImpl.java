package ro.bogdan.serverrmi.resource;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ro.bogdan.serverrmi.dto.MonitoredDataDto;
import ro.bogdan.serverrmi.exception.SensorNotFoundException;
import ro.bogdan.serverrmi.model.MonitoredData;
import ro.bogdan.serverrmi.model.Sensor;
import ro.bogdan.serverrmi.repository.SensorRepository;
import ro.bogdan.serverrmi.service.RMIService;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AutoJsonRpcServiceImpl
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RMIServiceImpl implements RMIService {

    private final SensorRepository sensorRepository;
    private final ModelMapper modelMapper;

    @Override
    public List<MonitoredDataDto> getData(String sensorUid) {
        Sensor sensor = sensorRepository.findByUid(sensorUid).orElseThrow(()-> new SensorNotFoundException(sensorUid));
        return sensor.getMonitoredDataSet().stream().map(this::fromEntityToDto).collect(Collectors.toList());
    }
    private MonitoredDataDto fromEntityToDto(MonitoredData monitoredData){
        MonitoredDataDto monitoredDataDto = modelMapper.map(monitoredData,MonitoredDataDto.class);
        if(monitoredData.getSensor()!=null)
            monitoredDataDto.setSensorUid(monitoredData.getSensor().getUid());
        return monitoredDataDto;
    }
}
