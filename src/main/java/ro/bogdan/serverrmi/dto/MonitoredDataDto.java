package ro.bogdan.serverrmi.dto;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class MonitoredDataDto {
    private Date date;
    private Integer energyConsumption;
    private String sensorUid;
}
