package ro.bogdan.serverrmi.exception;

public class SensorNotFoundException  extends RuntimeException{
    public SensorNotFoundException(String uid){
        super("Could not find sensor with following uid: "+uid);
    }
}
