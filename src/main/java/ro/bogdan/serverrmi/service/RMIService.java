package ro.bogdan.serverrmi.service;

import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;
import ro.bogdan.serverrmi.dto.MonitoredDataDto;
import ro.bogdan.serverrmi.model.MonitoredData;

import java.util.List;

@JsonRpcService("/data")
public interface RMIService {
    List<MonitoredDataDto> getData(@JsonRpcParam(value = "sensorUid") String sensorUid);
}
