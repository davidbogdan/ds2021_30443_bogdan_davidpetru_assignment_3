package ro.bogdan.serverrmi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerRmiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerRmiApplication.class, args);
    }

}
