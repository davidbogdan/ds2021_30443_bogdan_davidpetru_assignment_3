package ro.bogdan.serverrmi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.bogdan.serverrmi.model.MonitoredData;

@Repository
public interface MonitoredDataRepository  extends JpaRepository<MonitoredData,Long> {
}
